import System.Environment {- base -}

import Sound.Sc3.Ui.Sclang.Control {- hsc3-ui -}

help :: [String]
help =
  [ "hsc3-ui command arguments..."
  , "  ctl scsyndef FILE-NAME"
  , "  ctl text TEXT"
  ]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["ctl", "scsyndef", fn] -> scsyndef_ui_run "ui" 1 fn
    ["ctl", "text", txt] -> control_text_ui_run "ui" 1 txt
    _ -> putStrLn (unlines help)
