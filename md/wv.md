# wv

Gtk3/Webkit2 minimal viewer, <https://github.com/zserge/webview>

~~~~
$ hsc3-wv -h
wv uri width height resizeable
$ cat /tmp/t.html
<html><body><h1>h</h1><p>p</p></body></html>
$ hsc3-wv file:///tmp/t.html 500 300 false
...
$
~~~~
