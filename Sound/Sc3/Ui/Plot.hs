-- | Plots
module Sound.Sc3.Ui.Plot where

import System.Process {- process -}

{- | <http://www.baudline.com/manual/options.html>

> ui_baudline 4096 50 "linear" 2
-}
ui_baudline :: Int -> Int -> String -> Int -> IO ()
ui_baudline fft_size overlap space threads =
  let opt =
        [ "-jack"
        , "-inconnect"
        , "SuperCollider"
        , "-fftsize"
        , show fft_size
        , "-overlap"
        , show overlap
        , "-record"
        , "-space"
        , space
        , "-threads"
        , show threads
        ]
  in spawnProcess "baudline_jack" opt >> return ()

{- | <https://rohandrape.net/?t=rju&e=md/rju-scope.md>

> ui_scope_rju 512 100.0 ("embed",9) 2 (512,512) 4.0
-}
ui_scope_rju :: Int -> Double -> (String, Int) -> Int -> (Int, Int) -> Double -> IO ()
ui_scope_rju sz dt (md, em) nc (w, h) gn =
  let opt =
        [ "-b"
        , show sz
        , "-d"
        , show dt
        , "-e"
        , show em
        , "-g"
        , show gn
        , "-h"
        , show h
        , "-m"
        , md
        , "-n"
        , show nc
        , "-p"
        , "SuperCollider:out_%d"
        , "-w"
        , show w
        ]
  in spawnProcess "rju-scope" opt >> return ()
