-- | Ugen Control interface
module Sound.Sc3.Ui.Sclang.Control where

import Control.Monad {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Safe {- safe -}
import Text.Printf {- base -}

import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Server.Graphdef.Binary as Graphdef {- hsc3 -}
import qualified Sound.Sc3.Server.Graphdef.Read as Graphdef.Read {- hsc3 -}
import qualified Sound.Sc3.Ugen.Graph as Graph {- hsc3 -}

import qualified Sound.Sc3.Ui.Sclang as Sclang {- hsc3-ui -}

import qualified Text.Regex as Regex {- regex-compat -}

-- * Control

-- | 'controlGroup' of 'controlMeta'
control_meta_group :: Control -> Maybe Control_Group
control_meta_group = join . fmap controlGroup . controlMeta

control_meta_group_err :: Control -> Control_Group
control_meta_group_err = fromMaybe (error "control_meta_group") . control_meta_group

control_set_split_group :: [Control] -> [[Control]]
control_set_split_group ls =
  case ls of
    [] -> []
    e : _ ->
      let (lhs, rhs) = splitAt (control_group_degree (control_meta_group_err e)) ls
      in lhs : control_set_split_group rhs

{- | Partition controls into either ordinary or grouped categories.
     The group controls must be named such that they are grouped when sorted in alphabetical sequence.
     The sequence is also sorted alphabetically.
-}
control_set_group :: Bool -> [Control] -> [Either Control [Control]]
control_set_group doSort c =
  let (p, q) = partition (isNothing . control_meta_group) c
      cmb = map Left p ++ map Right (control_set_split_group (sortOn controlName q))
  in (if doSort then sortOn (either controlName (controlName . Safe.headNote "?")) else id) cmb

{- | Lookup control meta data in Sc3 and Kyma dictionaries, else default to (0,1,lin)

>>> control_name_to_control_meta "q" == Control_Meta 0 10 "lin" 0 "" Nothing
True
-}
control_name_to_control_meta :: Fractional t => String -> Control_Meta t
control_name_to_control_meta nm =
  let get k = fmap (\(_, _, r) -> r) . find (\(p, _, _) -> k == p)
      grp = case last nm of
        '[' -> Just Control_Range
        ']' -> Just Control_Range
        'X' -> Just Control_Xy
        'Y' -> Just Control_Xy
        _ -> Nothing
  in case maybe (get nm kyma_event_value_ranges) Just (get nm sc3_control_spec) of
      Just (l, r, c) -> Control_Meta l r c 0 "" grp
      Nothing -> Control_Meta 0 1 "lin" 0 "" Nothing

-- | If Control does not have meta data, infer it.
control_with_inferred_meta :: Control -> Control
control_with_inferred_meta c =
  case controlMeta c of
    Nothing -> c {controlMeta = Just (control_name_to_control_meta (controlName c))}
    _ -> c

-- | Generate an sclang ControlSpec definition from 'Control_Meta' data.
control_meta_to_control_spec_txt :: Control_Meta Sample -> String
control_meta_to_control_spec_txt mt =
  case mt of
    Control_Meta lhs rhs wrp stp _un _grp ->
      printf
        "ControlSpec.new(minval: %.4f,maxval: %.4f,warp: '%s',step: %.4f)"
        lhs
        rhs
        wrp
        stp

-- _ -> error "control_meta_to_control_spec_txt?"

-- * Ui

{- | Trigger controls (ie. warp: "trigger") can be rendered as buttons.

> Control_U c = control_m kr "syncPh" 0 (0,1,"trigger")
> control_to_button_txt "w" 1 c
-}
control_to_button_txt :: String -> Node_Id -> Control -> Maybe String
control_to_button_txt wn nd ct =
  case ct of
    Control ControlRate _ix nm df _tr (Just (Control_Meta lhs rhs "trigger" _stp _un _grp)) _brk ->
      Just
        ( printf
            "Button.new(parent: %s, bounds: 100@15).states_([[\"%s\",Color.black,Color.grey(0.75)]]).mouseDownAction_({s.sendMsg('/n_set',%d,'%s',%f)}).action_({s.sendMsg('/n_set',%d,'%s',%f)}).value_(%.1f); %s.view.decorator.nextLine;"
            wn
            nm
            nd
            nm
            lhs
            nd
            nm
            rhs
            df
            wn
        )
    _ -> Nothing

{- | Switch controls (ie. warp: "sw") can be rendered as switches.

> Control_U c = control_m kr "gate" 0 (0,1,"switch")
> control_to_checkbox_txt "w" 1 c
-}
control_to_checkbox_txt :: String -> Node_Id -> Control -> Maybe String
control_to_checkbox_txt wn nd ct =
  case ct of
    Control ControlRate _ix nm df _tr (Just (Control_Meta 0 1 "switch" _stp _un _grp)) _brk ->
      Just
        ( printf
            "CheckBox.new(parent: %s, bounds: 100@15, text: \"%s\").action_({arg ch; s.sendMsg('/n_set',%d,'%s',ch.value.asInteger)}).value_(%.1f.asBoolean); %s.view.decorator.nextLine;"
            wn
            nm
            nd
            nm
            df
            wn
        )
    _ -> Nothing

-- | Generate an sclang EZSlider definition from 'Control' given the name of the parent view variable and 'Node_Id'.
control_to_ezslider_txt :: String -> Node_Id -> Control -> String
control_to_ezslider_txt wn nd ct =
  case ct of
    Control ControlRate _ix nm df False (Just mt) _brk ->
      printf
        "EZSlider.new(parent: %s,bounds: 700@15,label: '%s',controlSpec: %s,action:{arg z; s.sendMsg('/n_set',%d,'%s',z.value)},initVal: %.4f);"
        wn
        nm
        (control_meta_to_control_spec_txt mt)
        nd
        nm
        df
    _ -> error (show ("control_to_ezslider", wn, nd, ct))

{- | Generate an sclang EZRanger definition from a 'Control' pair given the name of the parent view variable and 'Node_Id'.

> (Control_U c1,Control_U c2) = control_rng kr "pan" (0,0.25) (-1,1,"lin")
> Sclang.sclang_eval_defer (control_grp_to_ezranger_txt "nil" 1 (c1,c2))
-}
control_grp_to_ezranger_txt :: String -> Node_Id -> (Control, Control) -> String
control_grp_to_ezranger_txt wn nd (c1, c2) =
  case (c1, c2) of
    (Control ControlRate _ix1 nm1 df1 False (Just mt1) _brk1, Control ControlRate _ix2 nm2 df2 False (Just _mt2) _brk2) ->
      printf
        "EZRanger.new(parent: %s,bounds: 700@15,label: '%s',controlSpec: %s,action:{arg z; s.sendMsg('/n_set',%d,'%s',z.value.[0],'%s',z.value[1])},initVal: [%.4f,%.4f]);"
        wn
        (nm1 ++ "]")
        (control_meta_to_control_spec_txt mt1)
        nd
        nm1
        nm2
        df1
        df2
    _ -> error "control_to_ezranger"

control_grp_to_ui_elem :: String -> Node_Id -> [Control] -> String
control_grp_to_ui_elem wn nd grp =
  case (control_meta_group_err (Safe.headNote "?" grp), grp) of
    (Control_Range, [p, q]) -> control_grp_to_ezranger_txt wn nd (p, q)
    (Control_Array _n, _ls) -> error "control_grp_to_ui_elem" -- multi-slider
    (Control_Xy, [_p, _q]) -> error "control_grp_to_ui_elem"
    _ -> error "control_grp_to_ui_elem"

-- | Control to either button or slider.
control_to_ui_elem_txt :: String -> Node_Id -> Control -> String
control_to_ui_elem_txt wn nd c =
  let sw = maybe (control_to_checkbox_txt wn nd c) Just (control_to_button_txt wn nd c)
  in maybe (control_to_ezslider_txt wn nd c) id sw

-- | Group 'Control' set and generate sclang code for an editor window.
control_set_ui_txt :: Bool -> String -> Node_Id -> [Control] -> [String]
control_set_ui_txt doSort nm nd ctl_set =
  let grp = control_set_group doSort ctl_set
      pre =
        [ printf "var rc = Rect.new(left: 100, top: Window.screenBounds.height - 100 - %d, width: 720, height: %d);" (length grp * 18) (length grp * 18)
        , printf "var w = Window.new(name:'%s',bounds: rc, resizable: false).front;" nm
        , "w.view.decorator=FlowLayout(w.view.bounds);"
        , "w.view.decorator.gap=2@2;"
        ]
  in pre ++ map (either (control_to_ui_elem_txt "w" nd) (control_grp_to_ui_elem "w" nd)) grp

control_set_ui_wr :: Bool -> FilePath -> String -> Node_Id -> [Control] -> IO ()
control_set_ui_wr doSort fn nm nd =
  writeFile fn
    . unlines
    . control_set_ui_txt doSort nm nd
    . map control_with_inferred_meta

{- | Control set Ui run

> (Control_U c1) = control_m kr "vc" 0 (0,31,"lin")
> (Control_U c2) = control_m kr "gain" 1 (0,4,"amp")
> (Control_U c3,Control_U c4) = control_rng kr "pan" (0,0.25) (-1,1,"lin")
> control_set_ui_run False "ctl-set" 1 [c1,c2,c3,c4]
-}
control_set_ui_run :: Bool -> String -> Node_Id -> [Control] -> IO ()
control_set_ui_run doSort nm nd ct = do
  let fn = "/tmp/" ++ nm ++ ".scd"
  control_set_ui_wr doSort fn nm nd ct
  void (Sclang.sclang_load_defer fn)

-- | Traverse Ugen graph and collect Control set.
ugen_control_set :: Ugen -> [Control]
ugen_control_set =
  let f u st = case u of
        Control_U c -> (c : st)
        _ -> st
  in nub . ugenFoldr f []

{- | Collect Controls from Ugen graph and run Ui.

> let f = control_m kr "mnn" 69 (0,127,"lin")
> let a = control_m kr "amp" 0.25 (0,4,"amp")
> let u = sinOsc ar (midiCps f) 0 * a
> let (p1,p2) = control_rng kr "pan" (-1,1) (-1,1,"lin")
> let u = pan2 (sinOsc ar (midiCps f) 0) (sinOsc kr 0.1 0 `in_range` (p1,p2)) a
> ugen_ui_run "ui" 1 u
> ugen_control_set u
-}
ugen_ui_run :: String -> Node_Id -> Ugen -> IO ()
ugen_ui_run nm nd = control_set_ui_run True nm nd . ugen_control_set

scsyndef_load_control_set :: FilePath -> IO [Control]
scsyndef_load_control_set syn_fn = do
  gd <- Graphdef.read_graphdef_file syn_fn
  let (_, gr) = Graphdef.Read.graphdef_to_graph gd
      k = Graph.ug_controls gr
  return (map Graph.u_node_k_to_control k)

{- | Sc Synthdef Ui Run

> let syn_fn = "/home/rohan/sw/hsc3-graphs/db/41f7f937d4f0138e.scsyndef"
> let syn_fn = "/home/rohan/sw/hsc3-graphs/db/1bfe4a2958b21aa4.scsyndef"
> scsyndef_load_control_set syn_fn
> scsyndef_ui_run "ui" 1 syn_fn
-}
scsyndef_ui_run :: String -> Node_Id -> FilePath -> IO ()
scsyndef_ui_run nm nd sy = scsyndef_load_control_set sy >>= control_set_ui_run True nm nd

-- * Controls / Text

{- | Locate and parse text of the form ctl=controlSpecSequence.

>>> control_text_parse "ctl=pan[:0.1,-1,1,lin;pan]:0.1,-1,1,lin"
Just [("pan[",0.1,(-1.0,1.0,"lin")),("pan]",0.1,(-1.0,1.0,"lin"))]
-}
control_text_parse :: String -> Maybe [ControlSpec Double]
control_text_parse txt =
  let rx = Regex.mkRegex "ctl=[][a-z0-9.,;:-]*"
  in case Regex.matchRegexAll rx txt of
      Just (_, m, _, []) -> Just (control_spec_seq_parse (drop 4 m))
      _ -> Nothing

{- | Run Ui for text control definition.

> control_text_ui_run "ui" 1 "commentary ; ctl=freq:220,110,440,exp;amp:0.1,0,1,amp;pan:0,-1,1,lin"
> control_text_ui_run "ui" 1 "commentary ; ctl=pan[:-0.1,-1,1,lin;pan]:0.1,-1,1,lin"
-}
control_text_ui_run :: String -> Node_Id -> String -> IO ()
control_text_ui_run nm nd =
  control_set_ui_run False nm nd
    . map control_spec_to_control
    . maybe (error "control_text_ui_run") id
    . control_text_parse
