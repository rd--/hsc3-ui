-- | Sc Language
module Sound.Sc3.Ui.Sclang where

import Control.Monad {- base -}
import Data.List {- base -}
import Text.Printf {- base -}

import qualified Sound.Osc as Osc {- hosc -}
import qualified Sound.Osc.Text as Osc {- hosc -}

-- * Ipc

{- | Send expression to sclang process for evaluation over Osc to indicated Udp port.
  This requires that EvalListener (c.f. sc3-rdl) is running at sclang.
-}
sclang_eval_at :: Osc.OscSocketAddress -> String -> IO String
sclang_eval_at address txt = do
  let msg = Osc.Message "/eval" [Osc.string txt]
      ret m = case m of
        Osc.Message "/result" [Osc.AsciiString asc] -> Osc.ascii_to_string asc
        _ -> error "sclang_eval?"
  Osc.withTransport
    (Osc.openOscSocket address)
    (Osc.sendMessage msg >> Osc.waitReply "/result" >>= return . ret)

{- | 'sclang_eval_at' standard sclang host and port settings.

> sclang_eval "1 + 2"
-}
sclang_eval :: String -> IO String
sclang_eval = sclang_eval_at (Osc.Udp, "127.0.0.1", 57120)

-- | 'void' of 'sclang_eval'.
sclang_eval_ :: String -> IO ()
sclang_eval_ = void . sclang_eval

-- | 'sclang_eval' of {}.defer of expression.
sclang_eval_defer :: String -> IO String
sclang_eval_defer txt = sclang_eval (printf "{%s}.defer" txt)

-- | 'void' of 'sclang_eval_defer'
sclang_eval_defer_ :: String -> IO ()
sclang_eval_defer_ = void . sclang_eval_defer

-- | 'sclang_eval' of .load of filename.
sclang_load :: FilePath -> IO String
sclang_load fn = sclang_eval (printf "\"%s\".load" fn)

-- | 'sclang_eval_defer' of .load of filename.
sclang_load_defer :: FilePath -> IO String
sclang_load_defer fn = sclang_eval_defer (printf "\"%s\".load" fn)

-- * Scope

{- | Run Sc3 audio signal scope.

nc = number of channels, ix0 = index of first bus, sz = buffer size (frames),
zm = temporal (horizontal) zoom, ty = type (0=displaced,1=overlaid,2=lissajou)

> ui_sc3_scope 12 12000 (2 ^ 14) 1 "audio" 0
-}
ui_sc3_scope :: Int -> Int -> Int -> Double -> String -> Int -> IO ()
ui_sc3_scope nc ix0 sz zm rt ty =
  sclang_eval_defer_
    ( printf
        "Server.default.scope(numChannels: %d,index: %d,bufsize: %d,zoom: %f,rate: '%s').style_(%d)"
        nc
        ix0
        sz
        zm
        rt
        ty
    )

{- | Run Sc3 frequency (FFT) scope.

(w,h) = (width,height) ix = index of bus

> ui_sc3_scope_freq (600,400) 0
-}
ui_sc3_scope_freq :: (Int, Int) -> Int -> IO ()
ui_sc3_scope_freq (w, h) ix = sclang_eval_defer_ (printf "FreqScope.new(width: %d, height: %d, busNum: %d)" w h ix)

-- | Run Sc3 control signal plotter.
ui_sc3_ctl_plot :: Int -> Int -> Int -> Int -> Double -> Double -> IO ()
ui_sc3_ctl_plot nf nc ix0 pf dt dr =
  sclang_eval_defer_
    ( printf
        "CtlPlot.new(numFrames: %d, numChannels: %d, indexZero: %d, plotFrames: %d, delayTime: %f, drawRate: %f)"
        nf
        nc
        ix0
        pf
        dt
        dr
    )

-- | Run Sc3 scsynth command list.
ui_scsynth_command_list :: [Osc.Message] -> IO ()
ui_scsynth_command_list ls =
  sclang_eval_defer_
    ( printf
        "Ui.scsynthCommandListFudi(fudiPacket: \"%s\", onSelection: true, server: Server.default);"
        (intercalate "; " (map (Osc.showMessage (Just 4)) ls))
    )

-- * Image

{- | View image file in window.

> ui_view_image_file "/home/rohan/sw/stsc3/lib/svg/smalltalk-balloon.svg"
> ui_view_image_file "/home/rohan/sw/stsc3/lib/png/squeak-mouse.png"
-}
ui_view_image_file :: FilePath -> IO ()
ui_view_image_file fn = sclang_eval_defer_ (printf "Ui.viewImageFile(fileName:\"%s\");" fn)
