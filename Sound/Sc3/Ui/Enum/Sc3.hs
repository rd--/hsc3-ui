module Sound.Sc3.Ui.Enum.Sc3 where

import Sound.Osc {- hosc -}
import Sound.Osc.Text {- hosc -}

import Sound.Sc3 {- hsc3 -}

import Sound.Sc3.Ui.Enum.Html {- hsc3-ui -}

{- | List of Osc messages, send to scsynth on selection.

> ui_scsynth_command_list (map (\x -> n_set1 1 "freq" (55 * (x ** 2))) [1 .. 7])
-}
ui_scsynth_command_list :: [Message] -> IO ()
ui_scsynth_command_list ls =
  ui_command_list_
    (map (showMessage (Just 4)) ls)
    (\ix -> withSc3 (sendMessage (ls !! ix)))
