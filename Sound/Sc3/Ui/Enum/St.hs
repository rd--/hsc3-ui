-- | Enum / St
module Sound.Sc3.Ui.Enum.St where

import qualified Data.Map as Map {- containers -}

-- | Usr action (State, Ix) -> Io St
type Act st = ((st, (Int, Int)) -> IO st)

-- | (Row-Ix, (Txt, Val))
type EnumMap = Map.Map Int (String, Double)

-- | All Txt at EnumMap (in sequence).
enum_txt :: EnumMap -> [String]
enum_txt = map (fst . snd) . Map.toAscList

-- | EnumMap from (Txt,Val) sequence.
num_enum :: [(String, Double)] -> EnumMap
num_enum lst = Map.fromList (zip [0 ..] lst)

-- | EnumMap from Txt sequence, value is Row-Ix.
str_enum :: [String] -> EnumMap
str_enum lst = num_enum (zip lst [0 ..])

-- | (Col-Ix, (Sel-Ix, EnumMap))
type St = Map.Map Int (Int, EnumMap)

-- | All Txt for each Col in sequence.
st_txt :: St -> [[String]]
st_txt = map (enum_txt . snd . snd) . Map.toAscList

-- | Get (Sel-Ix, (Txt, Val)) for Col-Ix.
st_get :: St -> Int -> (Int, (String, Double))
st_get st i =
  let get = Map.findWithDefault (error "st_get")
      (j, e) = get i st
  in (j, get j e)

-- | Get Sel-Ix for Col-Ix.
st_get_ix :: St -> Int -> Int
st_get_ix st = fst . st_get st

-- | Get Val for Col-Ix.
st_get_val :: St -> Int -> Double
st_get_val st = snd . snd . st_get st

-- | Set Sel-Ix at Col-Ix.
st_set :: St -> Int -> Int -> St
st_set st i j =
  let f (_, e) = (j, e)
  in Map.adjust f i st

-- | Lift St action to Act St
st_act :: (St -> IO ()) -> Act St
st_act f (st0, (x, y)) = do
  let st = st_set st0 x y
  f st
  return st
