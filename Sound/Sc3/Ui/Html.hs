-- | Ui Html pretty printer
module Sound.Sc3.Ui.Html where

import qualified Data.Char as Char {- base -}
import Data.List {- base -}
import Text.Printf {- base -}

import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}

import qualified Text.Html.Minus as H {- html-minus -}

import Sound.Sc3.Ui.Wv {- hsc3-ui -}

with_commas :: [H.Content] -> [H.Content]
with_commas = intersperse (H.cdata ",")

in_markers :: String -> String -> [H.Content] -> [H.Content]
in_markers l r x = H.cdata l : x ++ [H.cdata r]

in_brackets :: [H.Content] -> [H.Content]
in_brackets = in_markers "[" "]"

in_paren :: [H.Content] -> [H.Content]
in_paren = in_markers "(" ")"

primitive_pp_html :: Primitive Ugen -> H.Content
primitive_pp_html (Primitive rt nm i _o sp _z _b) =
  let param = map Db.input_name (Db.ugen_inputs (Db.u_lookup_cs_err nm))
      add_param (p, q) = H.div [] [H.cdata (p ++ ":"), ugen_pp_html q]
      usr = ugen_user_name nm sp
  in H.details
      []
      ( H.summary [] [H.cdata (usr ++ "." ++ rateAbbrev rt)]
          : in_paren (with_commas (map add_param (zip (param ++ repeat "_") i)))
      )

control_pp_html :: Control -> H.Content
control_pp_html (Control rt _ix nm df _tr _m _b) =
  H.cdata (printf "NamedControl.%s(name:\"%s\",values:%.2f)" (map Char.toLower (show rt)) nm df)

proxy_pp_html :: Proxy Ugen -> H.Content
proxy_pp_html (Proxy p i) =
  let c = H.div [H.class_attr "proxy"] [H.cdata "C", H.sub [] [H.cdata (show i)], H.cdata_raw "&larr;"]
  in H.div [] [c, primitive_pp_html p]

ugen_pp_html :: Ugen -> H.Content
ugen_pp_html u =
  case u of
    Constant_U (Constant c _) -> H.cdata (real_pp 4 c)
    Control_U c -> control_pp_html c
    Mce_U m -> H.div [] (in_brackets (with_commas (map ugen_pp_html (mce_to_list m))))
    Primitive_U p -> primitive_pp_html p
    Proxy_U p -> proxy_pp_html p
    _ -> error "ugen_pp_html?"

pp_css :: [String]
pp_css =
  [ "body {font-family: monospace}"
  , "div {display: inline}"
  , "div.proxy {color: red}"
  , "details {margin-left: 0.5em}"
  , "summary {color: blue; display: inline}"
  , "details[open] {border: 1px dotted lightgrey}"
  ]

ugen_graph_pp_html :: String -> Ugen -> H.Element
ugen_graph_pp_html nm u =
  H.html
    [H.lang "en"]
    [ H.head [] [H.title [] [H.cdata nm], H.style [] [H.cdata (unlines pp_css)]]
    , H.body [] [ugen_pp_html u]
    ]

ugen_graph_pp_html_wr :: FilePath -> String -> Ugen -> IO ()
ugen_graph_pp_html_wr fn nm u = writeFile fn (H.renderHtml5_def (ugen_graph_pp_html nm u))

-- > ugen_graph_pp_html_wv (out 0 (pan2 (sinOsc ar 440 0) 0 1))
ugen_graph_pp_html_wv :: Ugen -> IO ()
ugen_graph_pp_html_wv u = do
  let nm = "ugen_graph_pp_html_wv"
      fn = "/tmp/" ++ nm ++ ".html"
  ugen_graph_pp_html_wr fn nm u
  ui_hsc3_wv_fn fn (200, 200) True
