-- | Run a system process and retrieve output.
module Sound.Sc3.Ui.Process where

import System.Exit {- base -}
import System.Process {- process -}

{- | 'readProcessWithExitCode' returning stdout, deleting trailing newline.

> ui_shell_process "ls" ["/tmp"] ""
> ui_shell_process "cat" [] "echo"
-}
ui_shell_process :: FilePath -> [String] -> String -> IO (Maybe String)
ui_shell_process prg arg inp = do
  let delete_trailing_newline :: String -> String
      delete_trailing_newline str = if last str == '\n' then take (length str - 1) str else str
  (c, r, _) <- readProcessWithExitCode prg arg inp
  case c of
    ExitSuccess -> return (Just (delete_trailing_newline r))
    _ -> return Nothing

{- | 'readProcessWithExitCode' as boolean.

> ui_shell_process_exit_status "test" ["-d"] "/tmp"
-}
ui_shell_process_exit_status :: FilePath -> [String] -> String -> IO Bool
ui_shell_process_exit_status prg arg inp = do
  (c, _, _) <- readProcessWithExitCode prg arg inp
  return (c == ExitSuccess)
