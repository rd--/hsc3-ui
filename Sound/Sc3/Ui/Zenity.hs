-- | Simple Ui elements using <https://help.gnome.org/users/zenity/stable/>
module Sound.Sc3.Ui.Zenity where

import Sound.Sc3.Ui.Process {- hsc3-ui -}

-- | Run zenity with indicated arguments and no standard input.
ui_zenity :: [String] -> IO (Maybe String)
ui_zenity arg = ui_shell_process "zenity" arg ""

{- | Select existing directory.

> ui_choose_directory "/home/rohan/sw/stsc3/"
-}
ui_choose_directory :: FilePath -> IO (Maybe FilePath)
ui_choose_directory fn = ui_zenity ["--file-selection", "--directory", "--filename", fn]

{- | Select existing file.

> ui_choose_file "/home/rohan/sw/stsc3/help/ugen/"
-}
ui_choose_file :: FilePath -> IO (Maybe FilePath)
ui_choose_file fn = ui_zenity ["--file-selection", "--filename", fn]

{- | Select new file (require confirmation to select existing file).

> ui_save_file "/home/rohan/sw/stsc3/help/"
-}
ui_save_file :: FilePath -> IO (Maybe FilePath)
ui_save_file fn = ui_zenity ["--file-selection", "--save", "--confirm-overwrite", "--filename", fn]

{- | Select item from list.

> ui_choose_from_list (words "select item from an array")
-}
ui_choose_from_list :: [String] -> IO (Maybe String)
ui_choose_from_list ls =
  ui_shell_process
    "zenity"
    ["--list", "--text", "Select:", "--column", "A", "--hide-header", "--width", "300", "--height", "500"]
    (unlines ls)

{- | Confirm or cancel.

> ui_confirm "Confirm?"
-}
ui_confirm :: String -> IO Bool
ui_confirm txt = ui_shell_process_exit_status "zenity" ["--question", "--text", txt] ""
