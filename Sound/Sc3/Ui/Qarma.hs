-- | Simple Ui elements using <https://github.com/luebking/qarma>
module Sound.Sc3.Ui.Qarma where

import Sound.Sc3.Ui.Process {- hsc3-ui -}

-- | Run qarma with indicated arguments and standard input.
ui_qarma_with_stdin :: [String] -> String -> IO (Maybe String)
ui_qarma_with_stdin arg inp = ui_shell_process "qarma" arg inp

-- | Run qarma with indicated arguments and no standard input.
ui_qarma :: [String] -> IO (Maybe String)
ui_qarma arg = ui_shell_process "qarma" arg ""

{- | Select existing directory.

> ui_choose_directory "/home/rohan/sw/stsc3/"
-}
ui_choose_directory :: FilePath -> IO (Maybe FilePath)
ui_choose_directory fn = ui_qarma ["--file-selection", "--directory", "--filename", fn]

{- | Select existing file.

> ui_choose_file "/home/rohan/sw/stsc3/help/ugen/"
-}
ui_choose_file :: FilePath -> IO (Maybe FilePath)
ui_choose_file fn = ui_qarma ["--file-selection", "--filename", fn]

{- | Select new or existing file (confirmation required for existing files).

> ui_save_file "/home/rohan/sw/stsc3/help/"
-}
ui_save_file :: FilePath -> IO (Maybe FilePath)
ui_save_file fn = ui_qarma ["--file-selection", "--save", "--filename", fn]

{- | Select color as hex string.

> ui_choose_colour "#75f"
-}
ui_choose_colour :: String -> IO (Maybe String)
ui_choose_colour clr = ui_qarma ["--color-selection", "--color", clr]

-- | Select date, ISO format (YYYY-MM-YY)
ui_choose_date :: IO (Maybe String)
ui_choose_date = ui_qarma ["--calendar", "--date-format=yyyy-MM-dd"]

{- | Choose date starting from indicated date, ISO format.

> ui_choose_date_from (1981,08,01) -- 1st of August 1981
-}
ui_choose_date_from :: (Int, Int, Int) -> IO (Maybe String)
ui_choose_date_from (y, m, d) =
  ui_qarma
    [ "--calendar"
    , "--date-format=yyyy-MM-dd"
    , "--year=" ++ show y
    , "--month=" ++ show m
    , "--day=" ++ show d
    ]

{- | Select item from list.

> ui_choose_from_list (words "select item from an array")
-}
ui_choose_from_list :: [String] -> IO (Maybe String)
ui_choose_from_list ls = ui_qarma (["--list", "--text", "Select:", "--hide-header"] ++ ls)

{- | Edit text (single line)

> ui_edit_string "a"
-}
ui_edit_string :: String -> IO (Maybe String)
ui_edit_string txt = ui_qarma ["--entry", "--text", "Edit text:", "--entry-text", txt]

{- | Edit text (multiple line)

> ui_edit_text "a\nb\nc"
-}
ui_edit_text :: String -> IO (Maybe String)
ui_edit_text = ui_qarma_with_stdin ["--text-info", "--editable", "--auto-scroll"]

{- | Confirm or cancel.

> ui_confirm "Confirm?"
-}
ui_confirm :: String -> IO Bool
ui_confirm txt = ui_shell_process_exit_status "qarma" ["--question", "--text", txt] ""
