{-
import Data.IORef {- base -}

import Control.Monad.IO.Class {- transformers -}

import qualified Data.Text as Text {- text -}

import qualified GI.Gtk as Gtk {- gi-gtk -}

ui_button :: MonadIO m => String -> (Gtk.Button -> Gtk.ButtonClickedCallback) -> m Gtk.Button
ui_button txt recv = do
  b <- Gtk.buttonNew
  Gtk.buttonSetLabel b (Text.pack txt)
  _ <- Gtk.onButtonClicked b (recv b)
  return b

ui_label :: MonadIO m => String -> m Gtk.Label
ui_label = Gtk.labelNew . Just . Text.pack

ui_layout w o e = do
  c <- Gtk.boxNew o 2
  Gtk.containerAdd w c
  mapM_ (Gtk.containerAdd c) e
  return c

-- > chooseFromButtons (words "select from list")
chooseFromButtons :: [String] -> IO (Maybe Int)
chooseFromButtons txt_seq = do
  z <- newIORef Nothing
  _ <- Gtk.init Nothing
  w <- Gtk.windowNew Gtk.WindowTypeToplevel
  Gtk.windowSetTitle w (Text.pack "Choose From")
  Gtk.windowSetResizable w False
  _ <- Gtk.onWidgetDestroy w Gtk.mainQuit
  let recv ix _ = writeIORef z (Just ix) >> Gtk.windowClose w
  b <- mapM (\(txt,ix) -> ui_button txt (recv ix)) (zip txt_seq [0..])
  _ <- ui_layout w Gtk.OrientationHorizontal b
  Gtk.widgetShowAll w
  Gtk.main
  readIORef z

-- > chooseItemFromButtons (words "select from list")
chooseItemFromButtons :: [String] -> IO (Maybe String)
chooseItemFromButtons txt_seq = do
  ix <- chooseFromButtons txt_seq
  return (fmap ((!!) txt_seq) ix)

-- > chooseFromList (words "select from list")
chooseFromList :: [String] -> IO (Maybe Int)
chooseFromList txt_seq = do
  z <- newIORef Nothing
  _ <- Gtk.init Nothing
  w <- Gtk.windowNew Gtk.WindowTypeToplevel
  Gtk.windowSetTitle w (Text.pack "Choose From")
  Gtk.windowSetDefaultSize w 200 355
  Gtk.windowSetResizable w False
  _ <- Gtk.onWidgetDestroy w Gtk.mainQuit
  c <- Gtk.boxNew Gtk.OrientationVertical 5
  Gtk.containerAdd w c
  l <- Gtk.listBoxNew
  Gtk.widgetSetVexpand l True
  --s <- Gtk.scrolledWindowNew Nothing Nothing
  --Gtk.scrolledWindowSetChild s (Just l) -- ‘GI.Gtk’ does not export ‘scrolledWindowSetChild’
  Gtk.containerAdd c l
  e <- mapM ui_label txt_seq
  mapM_ (\x -> Gtk.widgetSetHalign x Gtk.AlignStart) e
  mapM_ (Gtk.containerAdd l) e
  let recv sel _ = do
        r <- Gtk.listBoxGetSelectedRow l
        ix <- Gtk.listBoxRowGetIndex r
        writeIORef z (if sel then Just (fromIntegral ix) else Nothing)
        Gtk.windowClose w
  b_accept <- ui_button "Accept" (recv True)
  b_cancel <- ui_button "Cancel" (recv False)
  -- _ <- Gtk.onListBoxRowActivated l (\_ -> recv True ()) -- selects on single-click...
  _ <- ui_layout c Gtk.OrientationHorizontal [b_accept,b_cancel]
  Gtk.widgetShowAll w
  Gtk.main
  readIORef z

-- > chooseItemFromList (words "select an item from a list of strings")
-- > chooseItemFromList (map show [100 .. 200])
chooseItemFromList :: [String] -> IO (Maybe String)
chooseItemFromList txt_seq = do
  ix <- chooseFromList txt_seq
  return (fmap ((!!) txt_seq) ix)
-}
