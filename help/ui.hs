-- select existing directory
Ui.ui_choose_directory "/home/rohan/sw/stsc3/"

-- select existing file
Ui.ui_choose_file "/home/rohan/sw/stsc3/help/ugen/"

-- select new or existing file (confirmation required for existing files)
Ui.ui_save_file "/home/rohan/sw/stsc3/help/"

-- select color as hex string
Ui.ui_choose_colour "#25f"

-- select date in ISO format
Ui.ui_choose_date

-- select date in ISO format
Ui.ui_choose_date_from (1981,08,01)

-- select item from list
Ui.ui_choose_from_list (words "select item from an array")

-- edit text (single line)
Ui.ui_edit_string "text"

-- edit text (multiple line)
Ui.ui_edit_text "a\nbc\ndef"

-- confirm or cancel
Ui.ui_confirm "Confirm?"

-- view svg image file in window ; sclang
Ui.ui_view_image_file "/home/rohan/sw/stsc3/lib/svg/smalltalk-balloon.svg"

-- view png image file in window
Ui.ui_view_image_file "/home/rohan/sw/stsc3/lib/png/squeak-mouse.png"

-- list view to run scsynth commands
Ui.ui_scsynth_command_list (map (\x -> n_set1 1 "freq" (55 * (x ** 2))) [1 .. 7])
