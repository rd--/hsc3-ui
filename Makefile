all:
	echo "hsc3-ui"

clean:
	(cd cmd ; make clean)
	rm -Rf dist dist-newstyle *~

mk-cmd:
	(cd cmd ; make all install)

push-all:
	r.gitlab-push.sh hsc3-ui

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
